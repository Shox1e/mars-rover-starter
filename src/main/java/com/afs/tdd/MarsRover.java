package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {

        this.location = location;
    }
    public Location getLocation() {
        return location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        }
        if (command.equals(Command.Left)) {
            turnLeft();
        }
        if (command.equals(Command.Right)) {
            turnRight();
        }
    }

    public void executeBatchCommand(List<Command> commands) {
        for (Command command:commands) {
            executeCommand(command);
        }
    }

    private void turnRight() {
        if (this.location.getDirection().equals(Direction.North)) {
            location.setDirection(Direction.East);
        }
        else if (this.location.getDirection().equals(Direction.South)) {
            location.setDirection(Direction.West);
        }
        else if (this.location.getDirection().equals(Direction.East)) {
            location.setDirection(Direction.South);
        }
        else if (this.location.getDirection().equals(Direction.West)) {
            location.setDirection(Direction.North);
        }
    }

    private void turnLeft() {
        if (this.location.getDirection().equals(Direction.North)) {
            location.setDirection(Direction.West);
        }
        else if (this.location.getDirection().equals(Direction.South)) {
            location.setDirection(Direction.East);
        }
        else if (this.location.getDirection().equals(Direction.East)) {
            location.setDirection(Direction.North);
        }
        else if (this.location.getDirection().equals(Direction.West)) {
            location.setDirection(Direction.South);
        }
    }

    public void move() {
        if (this.location.getDirection().equals(Direction.North)) {
            location.setCoordinationY(location.getCoordinationY() + 1);
        }
        if (this.location.getDirection().equals(Direction.South)) {
            location.setCoordinationY(location.getCoordinationY() - 1);
        }
        if (this.location.getDirection().equals(Direction.East)) {
            location.setCoordinationX(location.getCoordinationX() + 1);
        }
        if (this.location.getDirection().equals(Direction.West)) {
            location.setCoordinationX(location.getCoordinationX() - 1);
        }
    }
}
