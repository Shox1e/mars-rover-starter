package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MarsRoverTest {
    @Test
    void should_change_location_to_0_1_N_when_executeCommand_given_location_0_0_N_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Move);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(1,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.North,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_1_S_when_executeCommand_given_location_0_0_S_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Move);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(-1,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.South,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_E_when_executeCommand_given_location_1_0_E_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Move);
        //then
        Assertions.assertEquals(1,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.East,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_1_0_W_when_executeCommand_given_location_0_0_W_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Move);
        //then
        Assertions.assertEquals(-1,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.West,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_W_when_executeCommand_given_location_0_0_N_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Left);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.West,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_E_when_executeCommand_given_location_0_0_S_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Left);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.East,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_N_when_executeCommand_given_location_0_0_E_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Left);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.North,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_S_when_executeCommand_given_location_0_0_W_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Left);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.South,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_E_when_executeCommand_given_location_0_0_N_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Right);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.East,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_W_when_executeCommand_given_location_0_0_S_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Right);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.West,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_S_when_executeCommand_given_location_0_0_E_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Right);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.South,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_N_when_executeCommand_given_location_0_0_W_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Right);
        //then
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(0,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.North,marsRover.getLocation().getDirection());
    }


    @Test
    void should_change_location_to_1_2_N_when_executeCommand_given_location_0_0_N_and_command_MRMLM() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        List<Command> commands = new LinkedList<>();
        commands.add(Command.Move);
        commands.add(Command.Right);
        commands.add(Command.Move);
        commands.add(Command.Left);
        commands.add(Command.Move);
        marsRover.executeBatchCommand(commands);
        //then
        Assertions.assertEquals(1,marsRover.getLocation().getCoordinationX());
        Assertions.assertEquals(2,marsRover.getLocation().getCoordinationY());
        Assertions.assertEquals(Direction.North,marsRover.getLocation().getDirection());
    }
}
